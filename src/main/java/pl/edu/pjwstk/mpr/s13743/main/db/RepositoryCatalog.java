package pl.edu.pjwstk.mpr.s13743.main.db;

public interface RepositoryCatalog {
	
	public AddressRepository addresses();
	public ClientDetailsRepository clients();
	public OrderItemRepository orderItems();
	public OrderRepository orders();
}
