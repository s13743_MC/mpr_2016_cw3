package pl.edu.pjwstk.mpr.s13743.main.db;

import java.sql.Connection;
import java.util.List;

public interface Repository<TEntity> {
	
	public Connection getConnection();
	
	public TEntity withId(int id);
	public List<TEntity> getAll();
	public int add(TEntity entity);
	public int modify(TEntity entity);
	public void remove();
}