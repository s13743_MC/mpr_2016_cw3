package pl.edu.pjwstk.mpr.s13743.main.db.repos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import pl.edu.pjwstk.mpr.s13743.main.db.ClientDetailsRepository;
import pl.edu.pjwstk.mpr.s13743.main.domain.ClientDetails;

public class HsqlClientDetailsRepository implements ClientDetailsRepository {

	private Connection connection;
	
	private String url = "jdbc:hsqldb:hsql://localhost/workdb";
	
	String selectByIdSql = "SELECT * FROM ClientDetails WHERE id=?";
	String selectAllSql = "SELECT id, name, surname, login FROM ClientDetails";
	String insertSql = "INSERT INTO ClientDetails (name, surname, login) VALUES (?, ?, ?)";
	String updatetSql = "UPDATE ClientDetails SET name=?, surname=?, login=? WHERE id=?";
	String deleteSql = "DELETE FROM ClientDetails";
	String selectByNameSql = "SELECT * FROM ClientDetails WHERE name=?";
	String selectBySurnameSql = "SELECT * FROM ClientDetails WHERE surname=?";
	
	private PreparedStatement clientDetailsWithIdStatement;
	private PreparedStatement getAllStatement;
	private PreparedStatement addStatement;
	private PreparedStatement updateStatement;
	private PreparedStatement deleteStatement;
	private PreparedStatement clientDetailsWithNameStatement;
	private PreparedStatement clientDetailsWithSurnameStatement;
	
	private String createTableClientDetails = ""
			+ "CREATE TABLE ClientDetails("
			+ "id bigint GENERATED BY DEFAULT AS IDENTITY,"
			+ "name varchar(20), "
			+ "surname varchar(20), "
			+ "login varchar(20))";
	
	private Statement statement;
	
	public HsqlClientDetailsRepository() {
		try {
			connection = DriverManager.getConnection(url);
			statement = connection.createStatement();
			
			ResultSet rs = connection.getMetaData().getTables(null, null, null, null);
			boolean tableExists = false;
			
			while (rs.next()) {
				if ("ClientDetails".equalsIgnoreCase(rs.getString("TABLE_NAME"))) {
					tableExists = true;
					break;
				}
			}
			
			if (!tableExists)
				statement.executeUpdate(createTableClientDetails);
			
			clientDetailsWithIdStatement = connection.prepareStatement(selectByIdSql);
			getAllStatement = connection.prepareStatement(selectAllSql);
			addStatement = connection.prepareStatement(insertSql);
			updateStatement = connection.prepareStatement(updatetSql);
			deleteStatement = connection.prepareStatement(deleteSql);
			clientDetailsWithNameStatement = connection.prepareStatement(selectByNameSql);
			clientDetailsWithSurnameStatement = connection.prepareStatement(selectBySurnameSql);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	public Connection getConnection() {
		return connection;
	}
	
	public ClientDetails withId(int id) {
		ClientDetails clientDetailsWithId = new ClientDetails();
		try {
			clientDetailsWithIdStatement.setLong(1, id);
			
			ResultSet rs = clientDetailsWithIdStatement.executeQuery();
			
			while(rs.next()){
				clientDetailsWithId.setId(rs.getLong("id"));
				clientDetailsWithId.setName(rs.getString("name"));
				clientDetailsWithId.setSurname(rs.getString("surname"));
				clientDetailsWithId.setLogin(rs.getString("login"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return clientDetailsWithId;
	}
	
	public List<ClientDetails> getAll() {
		List<ClientDetails> clientDetails = new ArrayList<ClientDetails>();
		
		try {
			ResultSet rs = getAllStatement.executeQuery();
			
			while (rs.next()) {
				ClientDetails c = new ClientDetails();
				c.setId(rs.getLong("id"));
				c.setName(rs.getString("name"));
				c.setSurname(rs.getString("surname"));
				c.setLogin(rs.getString("login"));
				clientDetails.add(c);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return clientDetails;
	}
	
	public int add(ClientDetails clientDetails) {
		int count = 0;
		
		try {
			addStatement.setString(1, clientDetails.getName());
			addStatement.setString(2, clientDetails.getSurname());
			addStatement.setString(3, clientDetails.getLogin());
			
			count = addStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return count;
	}
	
	public int modify(ClientDetails clientDetails) {
		int count = 0;
		try {
			updateStatement.setString(1, clientDetails.getName());
			updateStatement.setString(2, clientDetails.getSurname());
			updateStatement.setString(3, clientDetails.getLogin());
			updateStatement.setLong(4, clientDetails.getId());
			
			count = updateStatement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return count;
	}
	
	public void remove() {
		try {
			deleteStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public List<ClientDetails> withName(String name) {
		List<ClientDetails> clientDetailsWithName = new ArrayList<ClientDetails>();
		try {
			clientDetailsWithNameStatement.setString(1, name);
			
			ResultSet rs = clientDetailsWithNameStatement.executeQuery();
			
			while(rs.next()){
				ClientDetails c = new ClientDetails();
				
				c.setId(rs.getLong("id"));
				c.setName(rs.getString("name"));
				c.setSurname(rs.getString("surname"));
				c.setLogin(rs.getString("login"));
				
				clientDetailsWithName.add(c);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return clientDetailsWithName;
	}
	
	public List<ClientDetails> withSurname(String surname) {
		List<ClientDetails> clientDetailsWithSurname = new ArrayList<ClientDetails>();
		try {
			clientDetailsWithSurnameStatement.setString(1, surname);
			
			ResultSet rs = clientDetailsWithSurnameStatement.executeQuery();
			
			while(rs.next()){
				ClientDetails c = new ClientDetails();
				
				c.setId(rs.getLong("id"));
				c.setName(rs.getString("name"));
				c.setSurname(rs.getString("surname"));
				c.setLogin(rs.getString("login"));
				
				clientDetailsWithSurname.add(c);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return clientDetailsWithSurname;
	}
}
