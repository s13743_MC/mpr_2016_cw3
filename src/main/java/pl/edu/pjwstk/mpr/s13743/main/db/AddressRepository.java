package pl.edu.pjwstk.mpr.s13743.main.db;

import java.util.List;

import pl.edu.pjwstk.mpr.s13743.main.domain.Address;

public interface AddressRepository extends Repository<Address>{
	
	public List<Address> withCity(String city);
	public List<Address> withCountry(String country);

}
