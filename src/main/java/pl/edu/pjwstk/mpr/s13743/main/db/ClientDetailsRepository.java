package pl.edu.pjwstk.mpr.s13743.main.db;

import java.util.List;

import pl.edu.pjwstk.mpr.s13743.main.domain.ClientDetails;

public interface ClientDetailsRepository extends Repository<ClientDetails> {
	
	public List<ClientDetails> withName(String name);
	public List<ClientDetails> withSurname(String surname);
}
