package pl.edu.pjwstk.mpr.s13743.main.db;

import java.util.List;

import pl.edu.pjwstk.mpr.s13743.main.domain.ClientDetails;
import pl.edu.pjwstk.mpr.s13743.main.domain.Order;

public interface OrderRepository extends Repository<Order> {
	
	public List<Order> withClient (ClientDetails client);
}
