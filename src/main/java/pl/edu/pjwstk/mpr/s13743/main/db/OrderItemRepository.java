package pl.edu.pjwstk.mpr.s13743.main.db;

import java.util.List;

import pl.edu.pjwstk.mpr.s13743.main.domain.OrderItem;

public interface OrderItemRepository extends Repository<OrderItem> {
	
	public List<OrderItem> withName(String name);
	public List<OrderItem> withPrice(Double price);
}
