package pl.edu.pjwstk.mpr.s13743.main.db.catalogs;

import pl.edu.pjwstk.mpr.s13743.main.db.AddressRepository;
import pl.edu.pjwstk.mpr.s13743.main.db.ClientDetailsRepository;
import pl.edu.pjwstk.mpr.s13743.main.db.OrderItemRepository;
import pl.edu.pjwstk.mpr.s13743.main.db.OrderRepository;
import pl.edu.pjwstk.mpr.s13743.main.db.RepositoryCatalog;
import pl.edu.pjwstk.mpr.s13743.main.db.repos.HsqlAddressRepository;
import pl.edu.pjwstk.mpr.s13743.main.db.repos.HsqlClientDetailsRepository;
import pl.edu.pjwstk.mpr.s13743.main.db.repos.HsqlOrderItemRepository;
import pl.edu.pjwstk.mpr.s13743.main.db.repos.HsqlOrderRepository;

public class HsqlRepositoryCatalog implements RepositoryCatalog {

	public AddressRepository addresses() {
		return new HsqlAddressRepository();
	}
	
	public ClientDetailsRepository clients() {
		return new HsqlClientDetailsRepository();
	}
	
	public OrderItemRepository orderItems() {
		return new HsqlOrderItemRepository();
	}
	
	public OrderRepository orders() {
		return new HsqlOrderRepository();
	}

}
